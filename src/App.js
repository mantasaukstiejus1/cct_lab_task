import {useState} from "react";
import Header from "./components/Header";
import Faq from "./components/Faq";

function App() {
    const [faqs, setFaqs] = useState([
        {
            id: 1,
            title: 'Build test task',
            step1: 'Create repository',
            step2: 'Implement designs',
            step3: 'Implement functionality',
            open: false
        },
        {
            id: 2,
            title: 'Submit your test task',
            step1: 'Open email client',
            step2: 'Sent link with information to',
            email: 'careers@cornercasetech.com',
            open: false
        },
        {
            id: 3,
            title: 'Participate in tech interview',
            step1: 'Talk with HR',
            step2: 'Talk with Tech team',
            open: false
        },
        {
            id: 4,
            title: 'Receive answer',
            step1: 'Receive answers',
            step2: 'Start your IT career',
            open: true
        }
    ])

    const toggleFaq = (index) => {
        setFaqs(faqs.map((faq, i) => {
            if (i === index) {
                faq.open = !faq.open
            } else {
                faq.open = false
            }
            return faq
        }))
    }

    return (
        <div className="App">
            <Header/>
            <div className="faqs">
                {faqs.map((faq, i) => (
                    <Faq key={i} faq={faq} index={i} toggleFaq={toggleFaq}/>
                ))}
            </div>
        </div>
    );
}

export default App;
