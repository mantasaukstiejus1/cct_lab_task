import React from 'react';

const Faq = ({faq, index, toggleFaq}) => {
    return (
        <div
            className={"faq " + (faq.open ? 'open' : '')}
            key={index}
            onClick={() => toggleFaq(index)}
        >
            <div className="faq-title">
                    <div className='d-flex'>
                        <div className='faqId'>{faq.id}</div>
                        <div className='faq-title-text'>{faq.title}</div>
                    </div>
                    <div className='status'/>
            </div>
            <div className="faq-steps">
                <div className="step">
                    <i className="far fa-check-circle"/>
                    <div className='step-text'>{faq.step1}</div>
                </div>
                <div className="step">
                    <i className="far fa-check-circle"/>
                    <div className='step-text'>{faq.step2}</div>
                    {faq.email && <div className='step-text-email'>{faq.email}</div>}
                </div>
                <div className={faq.step3 ? 'step' : 'step-no-display'}>
                    <i className='far fa-check-circle'/>
                    <div className='step-text'>{faq.step3}</div>
                </div>
            </div>
        </div>

    );
}

export default Faq;